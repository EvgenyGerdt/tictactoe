package tictactoe;
import java.util.Scanner;

public class Main {

    public static boolean step = false;
    public static int count = 0;
    public static boolean xWinner = false, oWinner = false;
    public static boolean check;
    public static boolean draw;

    public static void paintGameField(char[][] gameField) {
        System.out.println("---------");

        for (char[] chars : gameField) {
            System.out.print("| ");
            for (int j = 0; j < gameField[0].length; j++) {
                System.out.print(chars[j] + " ");
            }
            System.out.print("|\n");
        }

        System.out.println("---------");
    }

    public static char[][] makePlayerStep(char[][] gameField) {

        try {
            Scanner sc = new Scanner(System.in);

            System.out.print("Enter the coordinates: ");
            int x = sc.nextInt(), y = sc.nextInt();

            if(x > 3 || x < 1 || y > 3 || y < 1) {
                System.out.println("Coordinates should be from 1 to 3!");
                makePlayerStep(gameField);
            }

            else {
                if(gameField[x - 1][y - 1] == ' ') {
                    if(step) {
                        gameField[x - 1][y - 1] = 'O';
                        step = false;
                    }
                    else {
                        gameField[x - 1][y - 1] = 'X';
                        step = true;
                    }
                }
                else {
                    System.out.println("This cell is occupied! Choose another one!");
                    makePlayerStep(gameField);
                }
            }
            checkResult(gameField);
        }
        catch (Exception e) {
            System.out.println("You should enter numbers!");
            makePlayerStep(gameField);
        }

        return gameField;
    }

    public static void checkResult(char[][] gameField) {

        if(checkDiagonal(gameField) || checkLines(gameField)) {
            check = true;
        }
        else {
            check = false;
        }
    }

    public static boolean checkLines(char[][] gameField) {
        boolean cols, rows;

        char oSymb = 'O', xSymb = 'X';

        for(int col = 0; col < 3; col++) {
            cols = true;
            rows = true;

            for(int row = 0; row < 3; row++) {
                cols &= (gameField[col][row] == oSymb);
                rows &= (gameField[row][col] == oSymb);
            }
            if(cols || rows) {
                oWinner = true;
                return true;
            }
        }

        for(int col = 0; col < 3; col++) {
            cols = true;
            rows = true;

            for(int row = 0; row < 3; row++) {
                cols &= (gameField[col][row] == xSymb);
                rows &= (gameField[row][col] == xSymb);
            }
            if(cols || rows) {
                xWinner = true;
                return true;
            }
        }
        return false;
    }

    public static boolean checkDiagonal(char[][] gameField) {
        boolean toRight, toLeft;
        toRight = true; toLeft = true;
        char oSymb = 'O', xSymb = 'X';

        for(int i = 0; i < gameField[0].length; i++) {
            toRight &= (gameField[i][i] == oSymb);
            toLeft &= (gameField[3 - i - 1][i] == oSymb);
        }

        if(toRight || toLeft) {
            oWinner = true;
            return true;
        }

        toRight = true; toLeft = true;

        for(int i = 0; i < gameField[0].length; i++) {
            toRight &= (gameField[i][i] == xSymb);
            toLeft &= (gameField[3 - i - 1][i] == xSymb);
        }

        if(toRight || toLeft) {
            xWinner = true;
            return true;
        }

        return false;
    }

    public static void main(String[] args) {
        boolean game = true;
        char[][] gameField = new char[3][3];

        for(int i = 0; i < gameField.length; i++) {
            for(int j = 0; j < gameField[0].length; j++) {
                gameField[i][j] = ' ';
            }
        }

        while(game) {
            paintGameField(gameField);

            makePlayerStep(gameField);
            count++;

            if(check) {
                game = false;
            }
            else if(count == 9) {
                game = false;
                draw = true;
            }
        }

        paintGameField(gameField);

        if(oWinner) {
            System.out.println("O wins");
        }
        else if(xWinner){
            System.out.println("X wins");
        }
        else if(draw) {
            System.out.println("Draw");
        }
    }
}
